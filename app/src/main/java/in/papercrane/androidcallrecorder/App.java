package in.papercrane.androidcallrecorder;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.facebook.stetho.Stetho;
import com.papercrane.androidcallrecorder.BuildConfig;
import com.papercrane.androidcallrecorder.R;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowLog;
import com.raizlabs.android.dbflow.config.FlowManager;

import in.papercrane.androidcallrecorder.internals.di.components.AppComponent;
import in.papercrane.androidcallrecorder.internals.di.components.DaggerAppComponent;
import in.papercrane.androidcallrecorder.internals.di.modules.ApplicationModule;
import in.papercrane.androidcallrecorder.internals.di.modules.NetworkModule;
import timber.log.Timber;

public class App extends Application {

    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(new FlowConfig.Builder(this).openDatabasesOnInit(true).build());
        debugSetup();
        mAppComponent = buildAppComponent();
        mAppComponent.inject(this);
    }

    private AppComponent buildAppComponent() {
        String BASE_URL = " ";
        return DaggerAppComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .networkModule(new NetworkModule(BASE_URL))
                .build();
    }

    private void debugSetup() {
        if (BuildConfig.DEBUG) {
            FlowLog.setMinimumLoggingLevel(FlowLog.Level.V);
            Stetho.initializeWithDefaults(this);
            Timber.plant(new Timber.DebugTree());
        }
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }


}
