package in.papercrane.androidcallrecorder.internals.base;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.raizlabs.android.dbflow.structure.BaseModel;

import java.security.PublicKey;
import java.util.Observable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.papercrane.androidcallrecorder.App;
import in.papercrane.androidcallrecorder.internals.di.components.AppComponent;
import io.reactivex.ObservableTransformer;

public abstract class BaseActivity<Presenter extends BasePresenter>
        extends AppCompatActivity implements BaseView {

    private Presenter mPresenter;
    private Unbinder mUnbinder;


    protected abstract void injectDependencies();

    protected abstract void init();

    public Presenter getPresenter() {
        return this.mPresenter;
    }

    @Inject
    public void setPresenter(Presenter presenter) {
        this.mPresenter = presenter;
    }

    @LayoutRes
    public abstract int setLayout();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        injectDependencies();
        setContentView(setLayout());
        mUnbinder = ButterKnife.bind(this);
        init();
        mPresenter.onCreate();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
        mUnbinder.unbind();
    }

    protected <T> ObservableTransformer<T, T> addToCompositeDisposible() {
        return mPresenter.addToCompositeDisposible();
    }

    public AppComponent getAppComponent() {
        return App.class.cast(getApplication()).getAppComponent();
    }
}
