package in.papercrane.androidcallrecorder.internals.di.components;


import android.app.Application;
import android.content.Context;

import com.google.android.gms.common.SignInButton;

import javax.inject.Singleton;

import dagger.Component;
import in.papercrane.androidcallrecorder.App;
import in.papercrane.androidcallrecorder.data.repository.UserRepository;
import in.papercrane.androidcallrecorder.internals.di.modules.ApplicationModule;
import in.papercrane.androidcallrecorder.internals.di.modules.NetworkModule;
import in.papercrane.androidcallrecorder.internals.di.modules.RepositoryModule;

@Singleton
@Component(modules = {NetworkModule.class,
        ApplicationModule.class,
        RepositoryModule.class})
public interface AppComponent {

    void inject(App app);

    Context getContext();

    UserRepository getUserRepository();
}
