package in.papercrane.androidcallrecorder.internals.di.modules;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.papercrane.androidcallrecorder.data.api.Api;
import in.papercrane.androidcallrecorder.data.repository.IUserRepository;
import in.papercrane.androidcallrecorder.data.repository.UserRepository;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    UserRepository provideUserRepository(Api api) {
        return new IUserRepository(api);
    }

}
