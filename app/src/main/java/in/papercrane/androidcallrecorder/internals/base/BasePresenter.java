package in.papercrane.androidcallrecorder.internals.base;

import javax.inject.Inject;

public abstract class BasePresenter<View extends BaseView> extends BaseRxClass {

    private View mBaseView;

    public void onCreate() {
    }

    ;

    public void onResume() {
    }

    ;

    void onDestroy() {
        unSubscribe();
    }

    protected View getView() {
        return this.mBaseView;
    }

    @Inject
    public void setBaseView(View baseView) {
        mBaseView = baseView;
    }

}
