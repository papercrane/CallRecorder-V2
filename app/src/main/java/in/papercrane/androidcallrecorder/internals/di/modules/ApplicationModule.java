package in.papercrane.androidcallrecorder.internals.di.modules;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.GestureDetector;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.papercrane.androidcallrecorder.App;

@Module
public class ApplicationModule {

    private final App mApplication;

    public ApplicationModule(App application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return this.mApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("callRecorder", Context.MODE_PRIVATE);
    }
}
