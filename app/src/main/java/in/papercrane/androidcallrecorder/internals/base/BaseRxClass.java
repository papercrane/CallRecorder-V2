package in.papercrane.androidcallrecorder.internals.base;

import io.reactivex.ObservableTransformer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

abstract class BaseRxClass {

    private CompositeDisposable mDisposible;

    protected void AddSubscription(Disposable disposable) {
        if (mDisposible == null) {
            mDisposible = new CompositeDisposable();
        }
        mDisposible.add(disposable);
    }

    protected void unSubscribe() {
        if (mDisposible != null && !mDisposible.isDisposed()) {
            mDisposible.dispose();
        }
    }

    protected <T> ObservableTransformer<T, T> addToCompositeDisposible() {
        return upstream -> upstream.doOnSubscribe(this::AddSubscription);
    }

}
