package in.papercrane.androidcallrecorder.internals.di.modules;


import android.media.session.MediaSession;
import android.test.suitebuilder.annotation.Suppress;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.Api;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.structure.ModelAdapter;

import java.lang.annotation.Retention;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private final String BASE_URL;

    public NetworkModule(String BASE_URL) {
        this.BASE_URL = BASE_URL;
    }

    @Singleton
    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @SuppressWarnings("ConstantConditions")
    @Singleton
    @Provides
    OkHttpClient ProvideOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addNetworkInterceptor(chain ->
                        chain.proceed(chain.request().newBuilder()
                                .addHeader("Accept", "application/json").build())
                )
                .addNetworkInterceptor(chain -> {

                    return chain.proceed(chain.request());
                })
                .addNetworkInterceptor(new StethoInterceptor());
        return builder.build();
    }

    @Singleton
    @Provides
    Gson provideGson() {
        final GsonBuilder gsonBuilder = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaredClass().equals(ModelAdapter.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }

                })
                .serializeNulls();
        return gsonBuilder.create();
    }

    @Singleton
    @Provides
    Retrofit provideRtrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson));
        return retrofit.build();
    }

    @Singleton
    @Provides
    in.papercrane.androidcallrecorder.data.api.Api provideApi(Retrofit retrofit) {
        return retrofit.create(in.papercrane.androidcallrecorder.data.api.Api.class);
    }

}
