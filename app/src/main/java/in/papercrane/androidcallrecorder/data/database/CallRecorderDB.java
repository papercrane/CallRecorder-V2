package in.papercrane.androidcallrecorder.data.database;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = CallRecorderDB.NAME, version = CallRecorderDB.VERSION)
public class CallRecorderDB {

    public static final String NAME = "callRecorder";
    public static final int VERSION = 1;

}
