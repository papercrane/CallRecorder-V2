package in.papercrane.androidcallrecorder.data.models;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.structure.BaseModel;

public class Records extends BaseModel{

    @PrimaryKey
    public String date;

    @Column
    public String phoneNumber;

    @Column
    public String recordingFile;

}
