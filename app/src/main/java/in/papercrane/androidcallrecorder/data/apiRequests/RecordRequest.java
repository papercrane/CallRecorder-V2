package in.papercrane.androidcallrecorder.data.apiRequests;

import java.io.File;

public class RecordRequest {

    public String date;
    public String phoneNumber;
    public File recordFile;

}
