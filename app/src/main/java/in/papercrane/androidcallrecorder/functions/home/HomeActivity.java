package in.papercrane.androidcallrecorder.functions.home;

import android.content.Context;
import android.content.Intent;
import android.icu.text.AlphabeticIndex;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.papercrane.androidcallrecorder.R;

import butterknife.BindView;
import in.papercrane.androidcallrecorder.functions.service.RecordService;
import in.papercrane.androidcallrecorder.internals.base.BaseActivity;

public class HomeActivity extends BaseActivity<HomePresenter>
        implements HomeContract.View {


    @BindView(R.id.tb_start)
    ToggleButton mToggle;

    boolean isChecked;
    View view;


    public static void start(Context context) {
        Intent starter = new Intent(context, HomeActivity.class);
        context.startActivity(starter);
    }

    @Override
    protected void injectDependencies() {
        DaggerHomeComponent.builder()
                .homeModule(new HomeModule(this))
                .appComponent(getAppComponent())
                .build()
                .inject(this);
    }

    @Override
    protected void init() {

        isChecked = ((ToggleButton) view).isChecked();

        if (isChecked) {

            RecordService.start(this);
            Toast.makeText(this, "RecordStarted", Toast.LENGTH_SHORT).show();

        } else {

            RecordService.stop(this);
            Toast.makeText(this, "RecordStopped", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        outState.putBoolean("checked", isChecked);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onRestoreInstanceState(savedInstanceState, persistentState);
        isChecked = savedInstanceState.getBoolean("checked");
    }

    @Override
    public int setLayout() {
        return R.layout.activity_home;
    }
}
