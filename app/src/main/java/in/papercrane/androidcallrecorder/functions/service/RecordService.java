package in.papercrane.androidcallrecorder.functions.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.icu.text.AlphabeticIndex;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.IBinder;
import android.provider.Telephony;
import android.support.annotation.Nullable;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.format.DateFormat;
import android.text.style.TtsSpan;

import java.io.File;
import java.io.IOException;
import java.util.Date;

public class RecordService extends Service {

    MediaRecorder mRecorder;
    File file;
    boolean recordStarted;


    public static void start(Context context) {
        Intent starter = new Intent(context, RecordService.class);
        context.startService(starter);
    }

    public static void stop(Context context) {
        Intent stopper = new Intent(context, RecordService.class);
        context.stopService(stopper);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);

        file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PODCASTS);
        Date date = new Date();
        CharSequence mChar = DateFormat.format("dd-MM-yyyy-hh-mm-ss", date.getTime());

        mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.VOICE_CALL);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mRecorder.setOutputFile(file.getAbsolutePath() + "/" + mChar + "/" + ".amr");

        TelephonyManager mManager = (TelephonyManager) getApplicationContext()
                .getSystemService(getApplicationContext().TELEPHONY_SERVICE);

        assert mManager != null;
        mManager.listen(new PhoneStateListener() {

            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                super.onCallStateChanged(state, incomingNumber);

                    if (TelephonyManager.CALL_STATE_IDLE == state && mRecorder == null) {
                        mRecorder.stop();
                        mRecorder.reset();
                        mRecorder.release();
                        recordStarted = false;
                        stopSelf();

                    } else if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                        try {
                            mRecorder.prepare();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        mRecorder.start();
                        recordStarted = true;
                    }

            }
        }, PhoneStateListener.LISTEN_CALL_STATE);

        return START_STICKY;
    }
}
