package in.papercrane.androidcallrecorder.functions.home;


import dagger.Component;
import in.papercrane.androidcallrecorder.internals.di.components.AppComponent;

@HomeScope
@Component(modules = HomeModule.class, dependencies = AppComponent.class)
interface HomeComponent  {

    void inject(HomeActivity homeActivity);

}
