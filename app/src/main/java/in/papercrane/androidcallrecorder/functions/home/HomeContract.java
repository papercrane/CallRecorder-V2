package in.papercrane.androidcallrecorder.functions.home;

import in.papercrane.androidcallrecorder.internals.base.BaseView;

public interface HomeContract {


    interface Presenter {

    }

    interface View extends BaseView {

    }
}
