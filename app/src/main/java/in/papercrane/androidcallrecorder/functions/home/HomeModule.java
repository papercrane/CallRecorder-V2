package in.papercrane.androidcallrecorder.functions.home;


import android.support.v7.view.menu.MenuView;

import dagger.Module;
import dagger.Provides;

@Module
class HomeModule {

    private HomeContract.View mView;

    HomeModule(HomeContract.View view) {
        mView = view;
    }

    @HomeScope
    @Provides
    HomeContract.View providePresenter() {
        return mView;
    }


}
