package in.papercrane.androidcallrecorder.functions.home;


import javax.inject.Inject;

import in.papercrane.androidcallrecorder.data.repository.UserRepository;
import in.papercrane.androidcallrecorder.internals.base.BasePresenter;

@HomeScope
class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter{

    public static final String TAG = "hoePresenter";
    private UserRepository mUserRepository;

    @Inject
    HomePresenter(UserRepository userRepository) {
        mUserRepository = userRepository;
    }


}
